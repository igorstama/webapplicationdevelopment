﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Flight
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FlightNumber { get; set; }
        public int Capacity { get; set; }

        public virtual ICollection<City> Cities { get; set; }
        public virtual ICollection<Airline> Airlines { get; set; }
        public virtual ICollection<Airport> Airports { get; set; }

    }
}