﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Country
    {
        public  int Id { get; set; }
        public string Name { get; set; }
        public string Population { get; set; }
        public int Code { get; set; }


        public virtual ICollection<City> Cities { get; set; }
        

       }
}